#include "satisfabilidade.hpp"

Satisfabilidade::Satisfabilidade(char *name) {
    s = new Sat(name);
}

Satisfabilidade::~Satisfabilidade() {
    delete s;
}

void Satisfabilidade::satisfabilidade() {
    s->satisfabilidade();
}

void Satisfabilidade::info() {
    cout << "Satisfabilidade" << endl;
    if(s->getsatisfazivel()) {
        cout << "As clausulas sao satisfaziveis" << endl;
        s->combinacao();
    }
    else
        cout << "As clausulas nao sao satisfaziveis" << endl;
}