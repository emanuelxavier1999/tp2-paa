#ifndef grafo_HPP
#define grafo_HPP

#include <iostream>
#include <string>
#include <stdlib.h>

class Grafo {
protected:
    unsigned int n;
    int **matriz, *melhor, tamMelhor;
public:
    Grafo (char* name);
    Grafo ();
    virtual ~Grafo();
    void print ();
    bool aresta(unsigned int a, unsigned int b);
    int getN();
    bool fazParteDoClique(int *vet, int v);
    void inverterGrafo();
    virtual void clique (int *vet, int h, int n, int tam);
    virtual void clique();
    void conjuntoIndependente();
    void info();
};

#endif