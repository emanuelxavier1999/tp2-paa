# tp2 PAA

## Objetivo
Este trabalho prático consiste na implementação de três programas. São eles Clique, Conjunto Independentes e Satisfabilidade 
- Satisfabilidade: Dada uma formula booleana na forma normal conjuntiva, encontre uma atribuição de valores-verdade às variáveis da fórmula que a torne verdadeira, ou informe que não existe tal atribuição.
- Clique: Dado um grafo, encontre um conjunto máximo de vértices tal que todas as possíveis arestas entre eles estejam presentes.
- Conjunto independente: Dado um grafo, o objetivo é encontrar o maior número de vértices independentes, isto é, não existe aresta entre nenhum par deles.

- O Cliquedeve ser resolvido usando Branch and Bounde outros dois devem ser resolvidos por meio de reduções polinomiais.

## Compilar

> g++ main.cpp clique.cpp independente.cpp satisfabilidade.cpp util/grafo.cpp util/sat.cpp -Wall -o exe

## Executar

> ./exe metodo endereco/nome_do_arquivo.txt

### Metodos
0. Clique
1. Conjunto independente
2. Satisfabilidade