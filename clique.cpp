#include "./clique.hpp"

Clique::Clique(char name[]) {
    g = new Grafo(name);
}

Clique::~Clique() {
   delete g; 
}  

void Clique::clique() {
    g->clique();
}

void Clique::info() {
    std::cout << "Clique" << std::endl;
    g->info();
}