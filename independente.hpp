#ifndef independente_HPP
#define independente_HPP

using namespace std;

#include "./util/grafo.hpp"

class ConjuntoIndependente {
    Grafo *g;
public:
    ConjuntoIndependente(char name[]);
    ~ConjuntoIndependente();
    void conjuntoIndependente();
    void info();
};

#endif